/*
	###BOOT FUNCTIONS###
*/
console.log("Popup javascript file has now been loaded.");
siteEndpoint();
userAPIKey();
currentTabClipper();

console.log("Dumping all storage for debugging purposes.");
showAllStorage()

/*
	Contacts the API endpoint to see if the user is signed into the website or not.
	If they are not it'll load a button directing them to login.
	If they are it'll load the basics form to webclip their current page.
*/
function siteEndpoint(){
	chrome.storage.local.get('custom_site_endpoint', function(result) {		
		var storage_result = result.custom_site_endpoint;
		
		if(storage_result == null || storage_result == 'null'){
			console.log("No endpoint set. Adding default text.");
				$('#site_endpoint').val("Enter API Endpoint here.");
				$('#site_endpoint').before('<p><strong>Web clipping will not work if you do not have an endpoint entered.</strong></p>');
		}else{
			$('#site_endpoint').val(storage_result);
		}
    });
}

/*
	See if the user has entered their API key. In the event they have we'll add it to the form.
	If they have not we'll let them know they can enter it.
*/
function userAPIKey(){
	chrome.storage.local.get('user_api_key', function(result) {		
		var storage_result = result.user_api_key;
		
		if(storage_result == null || storage_result == 'null'){
			console.log("The user has not entered their API key.");
				$('#api_key').val("Retrieve your API key and paste it here to start posting.");
				$('#api_key').before('<p><strong>Web clipping will not work if you do not enter a valid API key.</strong></p>');
		}else{
			$('#api_key').val(storage_result);
		}
    });	
}

/*
	Main Clipper Form Validation and Filler
	This function will inspect the URL of the current tab and then fill in all of the data to the form contained in the popup.
	The user can then validate this data and submit it for clipping if everything looks good.
*/
function currentTabClipper(){
	chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
		var url = tabs[0].url;
		var raw_content_source = extractHostname(url);
		var is_image = URLImageCheck(url);
		content_source = raw_content_source.split('.')[0];

		//If the host starts with www. we want to remove the sub domain and get the actual core host name.
		if(content_source == 'www'){
			content_source = raw_content_source.split('.')[1];
		}

		//If is image has returned true we know we're going to want to upload static content.
		if(is_image){
			content_source = 'static';
		}
		
		//Pull in all of the valid mediums for submission from the form in the popup window.
		var valid_mediums = [];
		$('#source_medium > option').each(function(){
			valid_mediums.push($(this).val());
		});

		if(valid_mediums.includes(content_source)){
			$("#source_medium").val(content_source);
			$('#source_url').val(url);
		}else{
			$("#source_medium").before('<p><strong>It appears this source is not valid!</strong></p>');
			$("#source_medium").before('<p>The URL you are trying to webclip might not be supported. You may want to double check before submitting to avoid erros.</p>');
		}
	});
}

function submitClippedPost(){
	var endpoint_url = $('#site_endpoint').val() + 'api/web_clip';
	var source = $('#source_medium').val();
	var content_url = $('#source_url').val();
	var api_token = $('#api_key').val();
	
	if(isUrl(endpoint_url)){
		$.post(endpoint_url, { 'content_source':source, 'content_url':content_url, 'api_token': api_token}, 
			function(returnedData){
				console.log(returnedData);
				
				var response_data = JSON.parse(returnedData);
				
				if(response_data.success == true || response_data.success == 'true'){
					var full_preview_url = $('#site_endpoint').val() + "web_clip_highlight_preview/" + response_data.return;
					chrome.tabs.create({ url: full_preview_url });
					$('#working_update').remove();
				}else{
					$('#working_update').text("Something went wrong! Please check your post data or contract the site administrator.");
				}
		}).fail(function(){
			  $('#main_block').prepend('<h3>THERE WAS AN INTERNAL ERROR WHEN SUBMITTING THIS URL!</h3>');
		});
	}else{
		$('#main_block').prepend('<p>It appears your endpoint URL is not correctly configured. Please do so and try again.</p>');
	}
}

/*
	A click listener to see if the endpoint URL has been updated or not.
*/
$("#save_endpoint").click(function() {
  var endpoint_url = $('#site_endpoint').val();
  
  if(isUrl(endpoint_url)){
	chrome.storage.local.set({'custom_site_endpoint': endpoint_url}, function() {
		location.reload();
    });
  }else{
	  alert('Endpoint URL is not a valid URL and will not be saved. Please check you have entered a real URL.');
  }
  
  showAllStorage();
});

/*
	A click listener to see if the user has an API key entered or not.
*/
$("#save_api_key").click(function() {
	var api_key_val = $('#api_key').val();
  
	chrome.storage.local.set({'user_api_key': api_key_val}, function() {
		location.reload();
    });
  
	showAllStorage();
});

/*
	A click listeners to wait for when the users wants to submit a post to be saved to the running archive on their endpoint.
*/
$("#clip_this_page").click(function() {
	submitClippedPost();
	
	$('#main_block').prepend('<div class="container" id="working_update">Getting Your Post Ready. Give Us A Second.</div>');
});


/*
	###HELPER FUNCTIONS###
	A set of functions to help with small validation bits that might be needed.
*/
function isUrl(s) {
   var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
   return regexp.test(s);
}

function showAllStorage(){
	console.log("Tell me everything in storage.");
	chrome.storage.local.get(function(result){console.log(result)});
}

function extractHostname(url){
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

	var url_parts = url.split('/');

	for(var i = 1; i <= url_parts.length;i++){
		console.log(url_parts[i]);
	}

    if (url.indexOf("//") > -1) {
        hostname = url.split('/')[2];
    }else{
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

function URLImageCheck(url) {
    if(url.match(/\.(jpeg|jpg|gif|png)$/) != null){
		return true;
	}
	
	return false;
}
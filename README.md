# The Highlights Arena Web Clipper Extension

This is a simple web clipper chrome extension for [Highlights Arena.](https://highlightsarena.com/) However anyone who is deploying [The Sports Report repository](https://gitlab.com/ToxicAirEvent/TheSportsReport) for their own purposes can use this extension. It has configurable endpoints and API keys which allow this Chrome extension to contact any site using the base repository.

Currently this extension is in the early working stages. It is usable but I'm sure all of the bugs aren't worked out, and the code was somewhat sloppily put together in an effort just to get things off the ground. Over time I intend to come back in and do some code cleanup.